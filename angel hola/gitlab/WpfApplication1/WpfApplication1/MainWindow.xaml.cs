﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using negocios;
using entidad;


namespace WpfApplication1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();  
        }
            entidad.entidad obje = new entidad.entidad();
            negocios.negocios objn = new negocios.negocios();
            catalogo llamada = new catalogo();
            registro LLAMADA = new registro();

        private void confirmar_Click(object sender, RoutedEventArgs e)
        {
            
            DataTable dt = new DataTable();

            obje.usuario = input_usuario.Text;
            obje.contraseña = contra.Password;

            dt = objn.usuarios(obje);

            if (dt.Rows.Count > 0)
            {
                obje.usuario = dt.Rows[0][0].ToString();
                obje.contraseña = dt.Rows[0][1].ToString();

                this.Close();
                llamada.Show();
            }
            else
            {
                error_inicio.Content = "El correo o la contraseña no son correctos";
                input_usuario.Text = "";
                contra.Password = "";
            }
        }

        private void registroBTN_Click(object sender, RoutedEventArgs e)
        {
            LLAMADA.Show();
            this.Close();
        }
    }
}
