﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Lógica de interacción para catalogo.xaml
    /// </summary>
    public partial class catalogo : Window
    {
        public catalogo()
        {
            InitializeComponent();
            InitializeComponent();
            var products = GetProducts();
            if (products.Count > 0)
                ListViewProducts.ItemsSource = products;
        }
        private List<Product> GetProducts()
        {
            return new List<Product>()
            {
                new Product("Zapato de Vestir",29,"/imagenes/1xd.png"),
                new Product("Zapatos Burros",40.99,"/imagenes/2.png"),

                new Product("Yinas Zapos",7.99,"/imagenes/3.png"),
                new Product("Zapato Pumas",35,"/imagenes/4.png"),

                new Product("Zapatos Burros",51,"/imagenes/5.png"),
                new Product("Zapato Deportivos",25,"/imagenes/6.png"),

                new Product("Zapato Tacos",47,"/imagenes/7.png"),
                new Product("Zapato Tenis",39.99,"/imagenes/8.png"),

                new Product("Zapato De Tacones",50.50,"/imagenes/9.jpg"),
                new Product("Zapato Tacones Bajos",40.99,"/imagenes/10.png"),

                new Product("Sandalias",14.99,"/imagenes/11.jpg"),
                new Product("Sandalia Chilin ",69.99,"/imagenes/12.png"),
            };
        }

        private void TBShow(object sender, RoutedEventArgs e)
        {

        }

        private void TBHide(object sender, RoutedEventArgs e)
        {

        }

        private void carro(object sender, RoutedEventArgs e)
        {
            Show();
            mostrar_catalogo mostr = new mostrar_catalogo();
            mostr.Show();
        }
    }
}
