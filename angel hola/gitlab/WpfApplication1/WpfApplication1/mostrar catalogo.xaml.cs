﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Lógica de interacción para mostrar_catalogo.xaml
    /// </summary>
    public partial class mostrar_catalogo : Window
    {
        public mostrar_catalogo()
        {
            InitializeComponent();
        }

        private void cata(object sender, RoutedEventArgs e)
        {
            Show();
            catalogo mostra = new catalogo();
            mostra.Show();
        }
    }
}
